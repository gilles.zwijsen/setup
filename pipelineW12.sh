#!/bin/bash

# Set the desired clone destination folder
DESTINATION_FOLDER="/home/max.etman"

# Set the SSH URL of the GitLab repository
GITLAB_REPO_URL="git@gitlab.com:Max.etman/website-for-infra-w12.git"

# Install git if not already installed
sudo apt-get install -y git

# Generate SSH key pair if not already generated
if [ ! -f ~/.ssh/id_rsa ]; then
  ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa <<<y
fi

# Add SSH key to GitLab account
echo "Copy the following SSH public key and add it to your GitLab account:"
echo ""
cat ~/.ssh/id_rsa.pub
echo ""

# Prompt for confirmation before cloning
read -rp "Press Enter to continue once you have added the SSH key to your GitLab account..."

#echo "testing access 1..."
#ssh -T hg@github.com
#eval `ssh-agent -s`
#ssh-add ~/.ssh/id_rsa
#ssh -T hg@github.com
#echo "testing access 2"

#git ls-remote --exit-code GITLAB_REPO_URL >/dev/null 2>&1
#if [ $? -eq 0 ]; then
#  echo "Access granted!"
#else
#  echo "Access denied!"
#fi
#git config user.email "max.etman@student.kdg.be"
#git config user.name "Max.etman"
# Clone the GitLab repository
#echo "Cloning GitLab repository..."
#cd "$DESTINATION_FOLDER"
#git clone "$GITLAB_REPO_URL"

# Provide GitLab credentials if prompted
#cd "$(basename "$GITLAB_REPO_URL" ".git")"


echo "Setup for cloning repository is a success, follow the manual steps to clone now"
