#!/bin/bash
dbservername="test-sql"
gcloud sql instances list &> /tmp/hassql
hassql=$(cat /tmp/hassql | grep $dbservername)
name="test-$(date +%s)"
local_ip=$(echo -n $(hostname -i))
dbname="database1"
dbuname="user"
dbpsswd="pass"
sqlfile_relative="setup.sql"
script_dir=$(dirname "$(realpath "$0")")

echo "$script_dir"
echo "$hassql"
sqlfile="$script_dir/$sqlfile_relative"



 checks if a database of name test-sql exists
if [ "$hassql" != '' ];
then
echo 'Database exists already!'
gcloud sql instances delete $dbservername
exit 100
fi

gcloud sql instances create $dbservername \
--project=$PROJECT_ID \
--tier=db-g1-small \
--region=europe-west1 \
--network=network-hihixd \
--database-version=POSTGRES_13 \
--no-assign-ip

echo "xd"

# retrieves the ip adresses from the VM and the sql server
# vmaddress=$(gcloud compute instances list | grep "$name" | tr -s " " | cut -d " " -f5)
sqladdress=$(gcloud sql instances list | grep "$dbservername" | tr -s " " | cut -d " " -f5)

# installs postgresql-client
#gcloud compute ssh $name --zone=europe-west1-b --command="sudo apt-get update; 
#    sudo apt-get install postgresql-client"

    # creates database with the name of the first input on the script
gcloud sql databases create $dbname --instance=$dbservername


# creates the user
gcloud sql users create $dbuname \
    --instance=$dbservername \
    --password=$dbpsswd


exit 0
