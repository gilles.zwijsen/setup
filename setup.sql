                                                                                                                                                                                                                                             
CREATE SCHEMA dbschema;

CREATE TABLE people (
    id int primary key,
    name VARCHAR(30),
    age int,
    isCool boolean
);
INSERT INTO people (id, name, age, isCool) VALUES (1, 'John Doe', 25, true);
INSERT INTO people (id, name, age, isCool) VALUES (2, 'Jane Smith', 32, true);
INSERT INTO people (id, name, age, isCool) VALUES (3, 'Michael Johnson', 40, false);
INSERT INTO people (id, name, age, isCool) VALUES (4, 'Sarah Williams', 28, true);
INSERT INTO people (id, name, age, isCool) VALUES (5, 'Robert Brown', 55, false);
INSERT INTO people (id, name, age, isCool) VALUES (6, 'Emily Davis', 19, true);
INSERT INTO people (id, name, age, isCool) VALUES (7, 'Daniel Anderson', 37, true);
